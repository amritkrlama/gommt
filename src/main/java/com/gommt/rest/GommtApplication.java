package com.gommt.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GommtApplication {

	public static void main(String[] args) {
		SpringApplication.run(GommtApplication.class, args);
	}
}
