package com.gommt.rest.service;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gommt.rest.dto.GIData;
import com.gommt.rest.exception.InvalidRequestException;
import com.gommt.rest.support.FileReader;

@RestController("/")
public class GOMMTService {
	
	@Autowired
	FileReader fileReader;
	private static final String FILE_NAME = "Corpus.CSV";
	
	@GetMapping("{key}")
	@ResponseBody
	public GIData getGIData(@PathVariable("key") String key) throws EncryptedDocumentException, InvalidFormatException, IOException, InvalidRequestException {
		String fileName = FILE_NAME;
		String home = System.getProperty("user.home");
		List<GIData> listGIData = fileReader.readGIDataFromFile(home+"/GOMMT/"+fileName+".XLSX");
		System.out.println(listGIData.size());
		GIData data = null;
		
		try {
			for(GIData tempData: listGIData) {
				if(tempData.getKey()!=null) {
					if(tempData.getKey().equalsIgnoreCase(key)) {
						data = tempData;
						break;
					}
				}
			}
		}catch(NullPointerException exception) {
			throw new InvalidRequestException("No record found for the key "+key);
		}
		
		return data;
	} 
}
