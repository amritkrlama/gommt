package com.gommt.rest.support;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Component;

import com.gommt.rest.dto.GIData;

@Component
public class FileReader {
	
	
	public List<GIData> readGIDataFromFile(String filePath) throws IOException, EncryptedDocumentException, InvalidFormatException{
		
		List<GIData> listGIData = new ArrayList<>();		
		Workbook workbook = WorkbookFactory.create(new File(filePath));
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = firstSheet.iterator();
		
		while(rowIterator.hasNext()) {
			Row nextRow = rowIterator.next();
			GIData data = null;
			if(nextRow.getRowNum()==0) {
				continue;
			}
			if(!checkIfRowIsEmpty(nextRow)) {
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				data = new GIData();
				
				while(cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					
					switch(columnIndex) {
					case 0:
						data.setKey((String)getCellValue(nextCell));
						break;
					case 1:
						data.setValue((double)getCellValue(nextCell));
						break;
					}
				}
			}
			listGIData.add(data);
		}
		workbook.close();
		return listGIData;
	}
	
	
	
	public Object getCellValue(Cell cell) {
	    switch (cell.getCellTypeEnum()) {
	    case STRING:
	        return cell.getStringCellValue();
	 
	    case BOOLEAN:
	        return cell.getBooleanCellValue();
	 
	    case NUMERIC:
	        return cell.getNumericCellValue();
	        
		default:
			break;
	    }
	 
	    return null;
	}
	
	
	private boolean checkIfRowIsEmpty(Row row) {
	    if (row == null) {
	        return true;
	    }
	    if (row.getLastCellNum() <= 0) {
	        return true;
	    }
	    for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
	        Cell cell = row.getCell(cellNum);
	        if (cell != null && cell.getCellTypeEnum() != CellType.BLANK) {
	            return false;
	        }
	    }
	    return true;
	}
}
