package com.gommt.rest.dto;

import org.springframework.stereotype.Component;

@Component
public class GIData {
	
	private String key;
	private double value;
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "GIData [key=" + key + ", value=" + value + "]";
	}
	
	
}
