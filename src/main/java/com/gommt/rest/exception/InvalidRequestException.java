package com.gommt.rest.exception;

public class InvalidRequestException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public InvalidRequestException(String message) {
		super(message);
	}
	
	public InvalidRequestException(String message, Throwable cause) {
		super(message, cause);
	}

}
