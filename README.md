# GOMMT #

This README documents the steps necessary to get the application up and running.

### What is this repository for? ###

* Task for GoIbibo (The application loads up an excel file having Key and Value pairs and exposes an API to access the Values of a given Key)
* V1.0

### Pre-requisites ###

* Java should be installed.
  (sudo apt-get install default-jdk)	
  
* Docker should be installed.
  ( sudo apt-get install docker-ce)
 

### How do I get set up? ###

* The "Corpus.CSV.XLSX" file should be stored in a local folder named "GOMMT".

* Build the image from Dockerfile (present in the root/target). Move to the folder root/target in the repository and execute the following command.
  docker build -t <image-name> .
  ex: docker build -t goibibo .
  
* Run the Container(image)
  docker run -i -t -v <path to the local folder where CSV file is stored>:/root/GOMMT -p 8080:8080 <image-name>
  ex: docker run -i -t -v /home/amrit/GOMMT:/root/GOMMT -p 8080:8080 goibibo
  
* To access the API, use the Postman collection below:
[![Run in Postman](https://run.pstmn.io/button.svg)]
(https://app.getpostman.com/run-collection/250aebd4e19199352e8e)


### Who do I talk to for suggestions or doubt? ###

* Amrit Kumar Lama (amritkrlama@gmail.com)